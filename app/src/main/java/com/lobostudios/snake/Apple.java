package com.lobostudios.snake;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.Random;

public class Apple {

    // The location x & y of the apple on the virtual grid, not in pixels in the screen
    private Point location = new Point();

    // The range of values we can choose from to spawn an apple
    private Point spawnRange;
    private int size;

    // Image to represent the apple
    private Bitmap bitmapApple;

    Apple(Context context, Point sr, int size) {
        // Make a note of the passed in spawn range
        spawnRange = sr;

        // Make a note of the size of an apple
        this.size = size;

        // Hide the apple off-screen until the game starts
        location.x = -10;

        // Load the image to the bitmap
        bitmapApple = BitmapFactory.decodeResource(context.getResources(), R.drawable.apple);

        // Resize the bitmap
        bitmapApple = Bitmap.createScaledBitmap(bitmapApple, size, size, false);
    }

    // This is called every time an apple is eaten
    void spawn() {
        // Choose two random values and place the apple
        Random random = new Random();
        location.x = random.nextInt(spawnRange.x) + 1;
        location.y = random.nextInt(spawnRange.y - 1) + 1;
    }

    // Let the snakegame know where the apple is. Snakegame can share this with the snake
    Point getLocation() {
        return location;
    }

    // Draw the apple
    void draw(Canvas canvas, Paint paint) {
        canvas.drawBitmap(bitmapApple, location.x * size, location.y * size, paint);
    }

}
