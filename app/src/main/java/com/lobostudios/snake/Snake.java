package com.lobostudios.snake;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;

import java.util.ArrayList;

public class Snake {

    // The location in the grid of all the segments
    private ArrayList<Point> segmentLocations;

    // How big is each segment of the snake?
    private int segmentSize;

    // How big is the entire grid
    private Point moveRange;

    // Where is the center of the screen horizontally in pixels?
    private int halfWayPoint;

    // For tracking movement heading
    private enum Heading {
        UP, RIGHT, DOWN, LEFT
    }

    // Start by heading to the right
    private Heading heading = Heading.RIGHT;

    // A bitmap for each direction the head can face
    private Bitmap bitmapHeadRight;
    private Bitmap bitmapHeadLeft;
    private Bitmap bitmapHeadUp;
    private Bitmap bitmapHeadDown;

    // A bitmap for the body
    private Bitmap bitmapBody;

    Snake(Context context, Point mr, int size) {
        // Initialize our ArrayList
        segmentLocations = new ArrayList<>();

        // Initialize the segment size and movement range from the passed in parameters
        segmentSize = size;
        moveRange = mr;

        // Create and scale the bitmaps
        bitmapHeadRight = BitmapFactory.decodeResource(context.getResources(), R.drawable.head_right);
        bitmapHeadLeft = BitmapFactory.decodeResource(context.getResources(), R.drawable.head_left);
        bitmapHeadUp = BitmapFactory.decodeResource(context.getResources(), R.drawable.head_up);
        bitmapHeadDown = BitmapFactory.decodeResource(context.getResources(), R.drawable.head_down);
        bitmapBody = BitmapFactory.decodeResource(context.getResources(), R.drawable.body);

        bitmapHeadRight = Bitmap.createScaledBitmap(bitmapHeadRight, size, size, false);
        bitmapHeadLeft = Bitmap.createScaledBitmap(bitmapHeadLeft, size, size, false);
        bitmapHeadUp = Bitmap.createScaledBitmap(bitmapHeadUp, size, size, false);
        bitmapHeadDown = Bitmap.createScaledBitmap(bitmapHeadDown, size, size, false);
        bitmapBody = Bitmap.createScaledBitmap(bitmapBody, size, size, false);

        // The halfway point across the screen in pixels. Used to detect which side of screen was pressed
        halfWayPoint = mr.x * size / 2;
    }

    // Tget the snake ready for a new game
    void reset(int w, int h) {
        // Reset the heading
        heading = Heading.RIGHT;

        // Delete the old contents of the arraylist
        segmentLocations.clear();

        // Start with a single snake segment, THE HEAD
        segmentLocations.add(new Point(w / 2, h / 2));
    }

    void move() {
        // Move the body. Start at the back and move it to the position of the segment in front of it
        for (int i = segmentLocations.size() - 1; i > 0; i--) {
            // Make it the same value as the next segment going forwards towards the head
            segmentLocations.get(i).x = segmentLocations.get(i - 1).x;
            segmentLocations.get(i).y = segmentLocations.get(i - 1).y;
        }

        // Move the head in the appropriate heading. Get the existing head position
        // Get the existing head position
        Point p = segmentLocations.get(0);

        // Move it appropriately
        switch (heading) {
            case UP:
                p.y--;
                break;

            case RIGHT:
                p.x++;
                break;
            case DOWN:
                p.y++;
                break;
            case LEFT:
                p.x--;
                break;
        }
        // Insert the adjusted point back into position 0
        segmentLocations.set(0, p);
    }

    boolean detectDeath() {
        // Has the snake died?
        boolean dead = false;

        // Hit any of the screen edges
        if (segmentLocations.get(0).x == -1 || segmentLocations.get(0).x > moveRange.x
                || segmentLocations.get(0).y == -1 || segmentLocations.get(0).y > moveRange.y) {
            dead = true;
        }

        // Eaten itself
        for (int i = segmentLocations.size() - 1; i > 0; i--) {
            // Have any of the sections collided with the head
            if (segmentLocations.get(0).x == segmentLocations.get(i).x
                    && segmentLocations.get(0).y == segmentLocations.get(i).y) {
                dead = true;
            }
        }

        return dead;
    }

    boolean checkDinner(Point l) {
        // if (snakeXs[0] == l.x && snakeYs[0] == l.y) {
        if (segmentLocations.get(0).x == l.x && segmentLocations.get(0).y == l.y) {
            // Add a new Point to the list located off-screen.
            // This is OK because on the enxt call to move it will take the position of the segment in front of it
            segmentLocations.add(new Point((-10), -10));
            return true;
        }
        return false;
    }

    void draw(Canvas canvas, Paint paint) {
        // Don't run this code if Arraylist has nothing in it
        if (!segmentLocations.isEmpty()) {
            // All the code from this method goes here
            switch (heading) {
                case RIGHT:
                    canvas.drawBitmap( bitmapHeadRight, segmentLocations.get(0).x * segmentSize,
                            segmentLocations.get(0).y *segmentSize, paint);
                    break;
                case LEFT:
                    canvas.drawBitmap( bitmapHeadLeft, segmentLocations.get(0).x * segmentSize,
                            segmentLocations.get(0).y *segmentSize, paint);
                    break;
                case UP:
                    canvas.drawBitmap( bitmapHeadUp, segmentLocations.get(0).x * segmentSize,
                            segmentLocations.get(0).y *segmentSize, paint);
                    break;
                case DOWN:
                    canvas.drawBitmap( bitmapHeadDown, segmentLocations.get(0).x * segmentSize,
                            segmentLocations.get(0).y *segmentSize, paint);
                    break;
            }

            // Draw the snake body on block at a time
            for (int i = 1; i < segmentLocations.size(); i++) {
                canvas.drawBitmap( bitmapBody, segmentLocations.get(i).x * segmentSize,
                        segmentLocations.get(i).y * segmentSize, paint);
            }
        }
    }

    void switchHeading(MotionEvent event) {
        // Is the tap on the right hand side?
        if (event.getX() >= halfWayPoint) {
            switch (heading) {
                // Rotate right
                case UP:
                    heading = Heading.RIGHT;
                    break;
                case RIGHT:
                    heading = Heading.DOWN;
                    break;
                case DOWN:
                    heading = Heading.LEFT;
                    break;
                case LEFT:
                    heading = Heading.UP;
                    break;
            }
        }
        if (event.getX() < halfWayPoint) {
            switch (heading) {
                // Rotate left
                case UP:
                    heading = Heading.LEFT;
                    break;
                case RIGHT:
                    heading = Heading.UP;
                    break;
                case DOWN:
                    heading = Heading.RIGHT;
                    break;
                case LEFT:
                    heading = Heading.DOWN;
                    break;
            }
        }
    }

}
