package com.lobostudios.snake;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

import java.io.IOException;

public class SnakeGame extends SurfaceView implements Runnable {

    // Objects for the game loop/thread
    private Thread thread = null;
    // Control pausing between updates
    private long nextFrameTime;
    // Is the game currently playing and or paused?
    private volatile boolean playing = false;
    private volatile boolean paused = true;

    // For playing sound effects
    private SoundPool soundPool;
    private int eatID = -1;
    private int crashID = -1;
    private int background = -1;

    // The size in segments of the playable area
    private final int NUM_BLOCKS_WIDE = 40;
    private int numBlocksHigh;

    // How many points does the player have
    private int score;

    // Objects for drawing
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private Paint paint;

    // A snake ssss
    private Snake snake;

    // An apple
    private Apple apple;

    // This is the constructor method that gets called from snakeActivity
    public SnakeGame(Context context, Point size) {
        super(context);

        // Work out how many pixels each block is
        int blockSize = size.x / NUM_BLOCKS_WIDE;
        // How many blocks of the same size will fit into the height
        numBlocksHigh = size.y / blockSize;

        // Initialize the soundpool
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage( AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            soundPool = new SoundPool.Builder().setMaxStreams(5).setAudioAttributes(audioAttributes).build();
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }
        try {
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            // Prepare the sounds in memory
            descriptor = assetManager.openFd("apple_eat.wav");
            eatID = soundPool.load(descriptor, 0);

            descriptor = assetManager.openFd("background.wav");
            background = soundPool.load(descriptor, 0);

            descriptor = assetManager.openFd("crash.wav");
            crashID = soundPool.load(descriptor, 0);

        } catch (IOException e) {
        }

        // Initialize the drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        // Call the constructors of our two game objects
        apple = new Apple(context, new Point(NUM_BLOCKS_WIDE, numBlocksHigh), blockSize);
        snake = new Snake(context, new Point(NUM_BLOCKS_WIDE, numBlocksHigh), blockSize);
    }

    public void newGame() {
        // Reset the snake
        snake.reset(NUM_BLOCKS_WIDE, numBlocksHigh);

        // Get the apple ready for dinner
        apple.spawn();

        // Reset the score

        score = 0;

        // Setup nextFrameTime so an update can triggered
        nextFrameTime = System.currentTimeMillis();
    }

    // Handles the game loop
    @Override
    public void run() {
        while (playing) {
            if (!paused) {
                // Update 10 times a second
                if (updateRequired()) {
                    update();
                }
            }
            draw();
        }
    }

    public boolean updateRequired() {
        // Run at 10 frames per second
        final long TARGET_FPS = 10;
        // There are 1000 ms in a second
        final long MILLIS_PER_SECOND = 1000;

        // Are we due to update the frame
        if (nextFrameTime <= System.currentTimeMillis()) {
            nextFrameTime = System.currentTimeMillis() + MILLIS_PER_SECOND / TARGET_FPS;

            return true;
        }

        return false;
    }

    public void update() {
        // Move the snake
        snake.move();

        // Did the head of the snake eat the apple?
        if (snake.checkDinner(apple.getLocation())) {
            // This reminds me of Edge of Tomorrow. One day the apple will be ready!
            apple.spawn();

            // Add to score
            score++;

            // Play a sound
            soundPool.play(eatID, 1, 1, 0, 0, 1);
        }

        // Did the snake die?
        if (snake.detectDeath()) {
            // Pause the game ready to start again
            soundPool.play(crashID, 1, 1, 0, 0, 1);

            paused = true;
        }
    }

    public void draw() {
        // Get a lock on the canvas
        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();

            // Fill the screen with a color
            canvas.drawColor(Color.argb(255, 26, 128, 182));

            // Set the size and color of the paint for the text
            paint.setColor(Color.argb(255, 255, 255, 255));
            paint.setTextSize(120);

            // Draw the score
            canvas.drawText("" + score, 20, 120, paint);

            // Draw the apple and the snake
            apple.draw( canvas, paint);
            snake.draw( canvas, paint);

            // Draw some text while paused
            if (paused) {
                // Set the size and color of paint for the text
                paint.setColor(Color.argb(255, 255, 255, 255));
                paint.setTextSize(250);

                // Draw the message we will give this an internation upgrade soon
                canvas.drawText("Tap to Play!", 200, 700, paint);
            }

            // Unlock the canvas to show graphics for this frame
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                if (paused) {
                    paused = false;
                    newGame();
                    // Don't want to process snake direction for this tap
                    return true;
                }

                // Let the snake class handle the input
                snake.switchHeading( event);

                break;

                default:
                    break;
        }
        return true;
    }

    public void pause() {
        playing = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void resume() {
        playing = true;
        thread = new Thread(this);
        thread.start();
    }
}
